<?php
/**
 *
 * @package    mahara
 * @subpackage blocktype-comment
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Feedback';
$string['description'] = 'A block to display feedback';
$string['ineditordescription'] = 'Feedback for this page will display here rather than in the footer of the page.';
